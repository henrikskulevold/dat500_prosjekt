## Sentiment analysis on Twitter data using Spark streaming and Hadoop mrjob

## Requirements

```bash
# Install requirements
pip install -r requirements.txt

# Update requirements.txt file if an package is added
pip install pipreqs

pipreqs . --force
```

## Folder structure.

The different folders contains different type of jobs. Most of the folders has their own readme with steps on how to run different jobs.

```bash
dat500_prosjekt
├── cpu_ram_usage
│   ├── Contains Cpu and Ram logges + the logs creates for all jobs
|
├── data
│   ├── Datasets used. Only a shortened version of the big dataset is here due to git limits
|
├── div
│   ├── Div text files with steps we took to get in the cluster and how we installes Spark
|
├── mrjobs
│   ├── The mrjobs made. Preprocessing and batch jobs + some utils and logs.
|
├── notebooks
│   ├── ML model script + comparism with textblob and AFINN wordlist.
|
└── results
|   ├── analysis of batch job outputs.
|
├── utils
│   ├── Contains a logger used by spark jobs. (script here is a little messy)
|
└── spark_jobs
    ├── Spark batch and streaming jobs + logs

```
