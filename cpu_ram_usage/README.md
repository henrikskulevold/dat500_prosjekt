### install requirements

```bash
pip install -r requirements.txt
```

### Usage

```bash
# General
python3 cpu_ram_usage.py <test_name>


# run temp test
python3 cpu_ram_usage.py test_del_this

# rm test logs
rm -rf logs/test_del_this/

## add to git
git pull && git add logs && git commit -m "added cpu log" && git push
```

Some tests we ran:

```bash
## Some tests done:
# batch preprocessing
python3 cpu_ram_usage.py covid_tweets_preprocessing

python3 cpu_ram_usage.py training_data_preprocessing

# Train ml model
python3 cpu_ram_usage.py train_ml_model_v2


# Mrjob bach analysis with 1 reducer
python3 cpu_ram_usage.py mrjob_batch

# Mrjob bach analysis with 1 reducer shortened
python3 cpu_ram_usage.py mrjob_batch_shortened


# Mrjob bach analysis with 2 reducer
python3 cpu_ram_usage.py mrjob_batch_2_reducers

# Spark bach analysis with 2 reducer
python3 cpu_ram_usage.py spark_batch_v2

# Spark bach analysis with 2 reducer shortened
python3 cpu_ram_usage.py spark_batch_v2_shortened


# Spark bach analysis with SQL
python3 cpu_ram_usage.py spark_batch_SQL_v2

# Spark bach analysis with SQL
python3 cpu_ram_usage.py spark_batch_SQL_v2_shortened

# Spark bach analysis with 1 reducer groupByKey
python3 cpu_ram_usage.py spark_batch_1_red_groupByKey_v2

# Spark bach analysis with 1 reducer groupByKey short
python3 cpu_ram_usage.py spark_batch_1_red_groupByKey_v2_shortened


# Spark streaming analysis
python3 cpu_ram_usage.py spark_stream_v2


# Spark streaming analysis using sample stream
python3 cpu_ram_usage.py spark_stream_v2_sample

python3 cpu_ram_usage.py spark_stream_v2_sample_notrigger
```
