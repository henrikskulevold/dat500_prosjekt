import socket
from tqdm import tqdm
from time import sleep
import psutil
import getpass
from datetime import datetime
import os


# command line arguments, to name a test
import sys

# total arguments
n = len(sys.argv)
print("Total arguments passed:", n)

if n < 2:
    print("Please provide a name for the test")
    exit()

test_name = sys.argv[1]
print("Test name: " + test_name)


def get_dns_domain():
    return socket.gethostname()


# print and log to file, because spark output is verbose
user = get_dns_domain()
print("User: " + user)
logs_folder = "./logs/" + test_name + "/" + user
if not os.path.exists(logs_folder):
    os.makedirs(logs_folder)


def save_to_log(time, cpu, ram):
    with open(logs_folder + "/usage.log", "a") as log:
        log.write(time + "\t" + cpu + "\t" + ram + "\t" + "\n")


def start_log(text):
    with open(logs_folder + "/usage.log", "a") as log:
        log.write(text + "\n")


start_log(
    f"\n-- NEW RUN, datetime: {datetime.now()} -- test_name: "+test_name+"\n")


with tqdm(total=100, desc='cpu%', position=1) as cpubar, tqdm(total=100, desc='ram%', position=0) as rambar:
    while True:
        rambar.n = psutil.virtual_memory().percent
        cpubar.n = psutil.cpu_percent()
        save_to_log(str(datetime.now()), str(cpubar.n), str(rambar.n))
        rambar.refresh()
        cpubar.refresh()
        sleep(0.5)
