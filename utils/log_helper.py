# print and log to file, because spark output is verbose
from datetime import datetime
import time
import os


class Logger():
    def __init__(self, name):
        self.PROGRAM_START_TIME = time.time()
        self.name = name
        self.training_log_folder = "./logs"
        if not os.path.exists(self.training_log_folder):
            os.makedirs(self.training_log_folder)
        self.print_and_log(f"\n-- NEW RUN, datetime: {datetime.now()} -- \n")

    def print_and_log(self, text):
        print("---")
        print(text)
        print("---")
        self.log(text)

    def log(self, text):
        with open(self.training_log_folder + "/" + self.name + ".log", "a") as log:
            log.write(text + "\n")

    def log_program_duration(self):
        self.print_and_log(
            f"Program took {time.time() - self.PROGRAM_START_TIME} seconds")
