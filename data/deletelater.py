
import pandas as pd
import sys
import pandas
import os


local_path = os.path.join(os.path.dirname(__file__), "./big")


# kombinerte alle datasettene til et dataset
if __name__ == "__main__":
    dfs = []
    for file in os.listdir(local_path):
        print(file)
        if file.endswith(".CSV"):
            dfs.append(pd.read_csv(os.path.join(local_path, file)))
    df = pd.concat(dfs)
    df.to_csv(local_path + "big_sampeled.csv", index=False)
