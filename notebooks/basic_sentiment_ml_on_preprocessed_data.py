#!/usr/bin/env python
# coding: utf-8

# # Spark ml: Training of an sentiment classifier on tweets
# Important notice: output in this notebook populated by a small test dataset.

# ### Import modules

# In[8]:


# for local usage
import findspark
findspark.init()


# In[9]:


from pyspark.sql import SQLContext
from pyspark.sql import SparkSession


# In[10]:


from pyspark.sql.types import StructField, StructType, StringType, IntegerType, ArrayType
from pyspark.sql.functions import udf,regexp_replace, col, split, slice, size, expr, array_join
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import HashingTF
from pyspark.ml import Pipeline


# Helper to create logs of the for the script:

# In[11]:


# # print and log to file, because spark output is verbose
import os
import sys
module_path = os.path.abspath(os.path.join('../utils'))
if module_path not in sys.path:
    sys.path.append(module_path)
from log_helper import Logger # type: ignore # noqa

logger = Logger("ml_model_training")


# ### Create spark session

# In[12]:


spark = SparkSession     .builder     .appName("Sentiment Classifier model training in Spark")     .getOrCreate()


# ### Read data file into Spark dataFrame

# In[29]:


schema = StructType([
        StructField("tweet_id", StringType()),
        StructField("target_and_tokens", StringType()),
    ])

file_path = "hdfs:///dat500_prosjekt/training_data_output" 
logger.print_and_log("Using preprocessed data")
tweets_df = spark.read.csv(file_path, sep="\t", schema=schema)

# local version:
# tweets_df = spark.read.csv("del_this.txt",sep="  ",schema=schema)# )

### Note: dident manage to convert the data directly when reading in, så needs to do the conversion manually

# clean value
tweets_df = tweets_df.withColumn("target_and_tokens", regexp_replace(regexp_replace("target_and_tokens", "\\[", ""), "\\]", "")) 
tweets_df = tweets_df.withColumn("target_and_tokens", regexp_replace(col("target_and_tokens"), "\"", ""))

# split target_and_tokens into target and tokens
tweets_df = tweets_df.withColumn("target_and_tokens", split(col("target_and_tokens"), ","))
tweets_df = tweets_df.withColumn("target", col("target_and_tokens")[0])
tweets_df = tweets_df.withColumn("target", col("target").cast(IntegerType()))
tweets_df = tweets_df.withColumn("tokens", expr("slice(target_and_tokens, 2, SIZE(target_and_tokens))"))
tweets_df = tweets_df.drop('target_and_tokens')

# remove empty data (local test showed that it could be size one of an empty string).
tweets_df = tweets_df.filter((size("tokens") > 1)| ((size("tokens") == 1) & (col("tokens")[0] != "")))

tweets_df.show(truncate=True, n=3,)
print(tweets_df.schema)
logger.print_and_log("Showed init dataset")


# ## Split data

# In[99]:


#divide data, 70% for training, 30% for testing
data_splitted = tweets_df.randomSplit([0.8, 0.2]) 
train_data = data_splitted[0]
test_data = data_splitted[1]
logger.print_and_log(f"Training data count: {train_data.count()}, Testing data count: {test_data.count()}")


# ### Create pipeline model

# In[104]:


# pipeline
hashTF = HashingTF(inputCol="tokens", outputCol="features")
lr = LogisticRegression(labelCol="target", featuresCol="features", 
                        maxIter=10, regParam=0.01)
regression_pipeline = Pipeline(stages= [hashTF, lr])


# ### Fit the model

# In[105]:


import time
from datetime import datetime
# start timer
start= time.time()

# traing model
logger.print_and_log(f"Start training at: {datetime.now()}")
model = regression_pipeline.fit(train_data)
logger.print_and_log(f"Training finnish at: {datetime.now()}")

# log training time
end = time.time()
duration = end - start
logger.print_and_log(f"Training is done, took {duration} seconds")


# ### Evaluate model on test data

# In[107]:


prediction = model.transform(test_data)
prediction = prediction.select(
    "tokens", "prediction", "target")
prediction.show(n=4, truncate = False)
correct_predictions = prediction.filter(
    prediction['prediction'] == prediction['target']).count()
total_predictions = prediction.count()

logger.print_and_log(f"Correct predictions (using ml model): {correct_predictions}, Total predictions: {total_predictions}, Accuracy: {correct_predictions/total_predictions}")


# ### Comparing the classifier to other tequnices

# In[123]:


# convert tokens to text
test_data = test_data.withColumn("text", array_join("tokens", " "))
test_data.show(n=2)


# Comparing to text blob

# In[124]:


# comparing to text blob
# Takes approks 40 sek, gets accuracy 0.6097772508891246
from textblob import TextBlob

test_data_textblob = test_data.withColumn("textblob",
      udf(lambda x: 0 if TextBlob(x).sentiment.polarity < 0 else 4,  IntegerType())(test_data.text))
test_data_textblob.show(n=2)

correct_predictions = test_data_textblob.filter(
    test_data_textblob['textblob'] == test_data_textblob['target']).count()
total_predictions = test_data_textblob.count()

logger.print_and_log(f"Correct predictions (using Textblob): {correct_predictions}, Total predictions: {total_predictions}, Accuracy: {correct_predictions/total_predictions}")


# Comparing to mrjob method

# In[125]:


# helper funcs, gottne from the mrjob. TOODO: make an module?
def get_words_scores():
    with open( "../mrjobs/assets/AFINN-en-165.txt") as f:
        words_dict = {}
        for line in f:
            word, score = line.split('\t')
            words_dict[word] = int(score)
    return words_dict
WORD_DICT = get_words_scores()

def _score_word(word):
    if word in WORD_DICT:
        return WORD_DICT[word]
    else:
        return 0

def _calc_tweet_sentiment(tweet):
    score = 0
    words = tweet.split()
    
    if (len(words) == 0):
        return 0    
    
    for word in words:
        score += _score_word(word)
    return score / len(words)

def get_tweet_sentiment(tweet):
    sentiment_score = _calc_tweet_sentiment(tweet)
    # retunere netrual på null her?
    if (sentiment_score < 0):
        return 0
    else:
        return 4


# In[127]:


test_data_dict_classification = test_data.withColumn("dict_classification",
      udf(lambda x: get_tweet_sentiment(x),  IntegerType())(test_data.text))
test_data_dict_classification.show(n=2)

correct_predictions = test_data_dict_classification.filter(
    test_data_dict_classification['dict_classification'] == test_data_dict_classification['target']).count()
total_predictions = test_data_dict_classification.count()

logger.print_and_log(f"Correct predictions (using Dictionary classification): {correct_predictions}, Total predictions: {total_predictions}, Accuracy: {correct_predictions/total_predictions}")


# ## Save model

# In[7]:


import os
models_folder_path = "./models/sentiment_models"

# Note: Used in get_model_version to get the model version. Dosent update when training in hadoop clusterm since it reads locally
if not os.path.exists(models_folder_path):
    os.makedirs(models_folder_path)


# In[8]:


def get_model_version(path):
    return len([name for name in os.listdir(path)])


# In[9]:


new_model_path = f"{models_folder_path}/v_{get_model_version(models_folder_path)}"
print(f"Saving model to {new_model_path}")


# In[133]:


model.save(new_model_path)


# ### Verify the saved model

# In[135]:


# load model
from pyspark.ml import PipelineModel

loaded_model = PipelineModel.load(new_model_path)
prediction = loaded_model.transform(test_data)
prediction = prediction.select(
    "tokens", "prediction", "target")
prediction.show(n=4, truncate = False)
correct_predictions = prediction.filter(
    prediction['prediction'] == prediction['target']).count()
total_predictions = prediction.count()

logger.print_and_log(f"Correct predictions (using loaded ml model): {correct_predictions}, Total predictions: {total_predictions}, Accuracy: {correct_predictions/total_predictions}")


# ### Test obvious sentiment text

# In[32]:


# create dataframe
schema = StructType([
        StructField("tweet_id", StringType()),
        StructField("target", IntegerType()),
        StructField("tokens", ArrayType(elementType=StringType())),
    ])
test_data_2 = spark.createDataFrame([
    (1, 1, ["love","this", "movie"]),
    (5, 1, ["great","day"]),
    (3, 1, ["training","tough", "but", "feels","really", "good", "after"]),
    (4, 0, ["thats","wierd"]),
    (2, 0, ["This","movie","terrible"]),
    (6, 0, ["horrible","song"])
], schema)
test_data_2.show(truncate = False)


# In[36]:


prediction = loaded_model.transform(test_data_2)
prediction = prediction.select(
    "tokens", "prediction", "target")
prediction.show(truncate = False)

logger.log("Predictions:")
logger.log(str(prediction.collect()))


# In[7]:


spark.stop()


# In[6]:


logger.log_program_duration()


# ### Convert to py file
# 
# ```bash
# jupyter nbconvert --to script basic_sentiment_ml_on_preprocessed_data.ipynb
# ```
