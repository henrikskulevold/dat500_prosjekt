### usage

Run traning of model using spark

```bash
# run ml training on preprocessed data
spark-submit basic_sentiment_ml_on_preprocessed_data.py

# remove trained model
hadoop fs -rm -R models/sentiment_models
```
