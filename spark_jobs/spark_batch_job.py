import os
from pyspark.ml import PipelineModel
from pyspark.ml.feature import HashingTF, Tokenizer, StopWordsRemover
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
from pyspark.sql.types import StructField, StructType, StringType, IntegerType, DateType
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import regexp_replace, col, explode, collect_list, slice
from collections import Counter

import os
import sys
module_path = os.path.abspath(os.path.join('../utils'))
if module_path not in sys.path:
    sys.path.append(module_path)
from log_helper import Logger  # type: ignore # noqa
logger = Logger("spark_batch_job")


# Init spark context
sc = SparkContext(appName="SparkBatchJob")
sqlContext = SQLContext(sc)

# Load ml model from HDFS
ML_MODEL_PATH = "./models/sentiment_models/v_0"
model = PipelineModel.load(ML_MODEL_PATH)

# load the output from the preprocessing job
input_file = "hdfs:///dat500_prosjekt/covid_tweets_shortened_output"
hadoop_rdd = sc.textFile(input_file)

# clean the text (since input file is readed with unusfull signs),
# and split key value
hadoop_rdd = hadoop_rdd.map(lambda line: line.replace(
    "\"", "").replace("[", "").replace("]", "").split("\t"))

# split the values to date and words
hadoop_rdd = hadoop_rdd.map(lambda line: (line[0], line[1].split(",")))

# convert to dataframe
df = hadoop_rdd.map(lambda line: Row(tweet_id=line[0],
                                     date=line[1][0],
                                     tokens=line[1][1:] if len(
                                         line[1]) > 1 else [],
                                     )).toDF()
# cast to correct types to remove timestamp
df = df.withColumn("date", df["date"].cast(DateType()))

# classify the tweets
predictions = model.transform(df)
predictions = predictions.select("tweet_id", "date", "tokens", "prediction")


"""
Different groupong methids:
"""


"""
First approach tested:
"""
# mapper function used in map reduce part under


# def map_func(row):
#     date, prediction, words, count = row[0][0], row[0][1], row[1][0], row[1][1]
#     words_counted = dict(Counter(words))
#     words_counted_top_20 = dict(
#         sorted(words_counted.items(), key=lambda x: x[1], reverse=True)[:20])

#     return (date, prediction), (count, words_counted_top_20)


# # Group by date and sentiment, and count most common words
# rdd = predictions.rdd \
#     .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), (row.tokens, 1)))
# # This line does not work well on large datasets, because of the concatinating of lists
# rdd = rdd.reduceByKey(lambda a, b: (a[0] + b[0], a[1] + b[1]))
# rdd = rdd.map(map_func)


# """
# Second approach tested:
# """
# exploded_df_on_words = (
#     predictions.withColumn("words", explode(predictions.tokens))
#     .groupBy(["date", "prediction", "words"])
#     .count()
#     .sort("count", ascending=False)
# )
# grouped_date_pred = exploded_df_on_words.groupBy(
#     ["date", "prediction"]).agg(collect_list("words"))
# grouped_top_10 = grouped_date_pred.withColumn(
#     "top_10_words", slice("collect_list(words)", start=1, length=20))
# grouped_top_10_filtered = grouped_top_10.select(
#     ["date", "prediction", "top_10_words"])
# rdd = grouped_top_10_filtered.rdd


# """
# Third approach tested:
# """


# def map_func(kv):
#     date, prediction, word_count_list, = kv[0][0], kv[0][1], kv[1]
#     words_counted_top_10 = dict(
#         sorted(word_count_list, key=lambda x: x[1], reverse=True)[:20])
#     return (date, prediction), words_counted_top_10


# # Maps to ((date, sentiment), tokens)
# rdd = predictions.rdd \
#     .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), row.tokens))
# # spread tokens to word and count accurance of (date, sentiment, word)
# rdd = rdd.flatMap(lambda kv: (((kv[0][0], kv[0][1], word), 1) for word in kv[1]))\
#     .reduceByKey(lambda a, b: a + b)  # output similar to first sql output
# # map and group to to (date, sentiment), [(word, count)]
# rdd = rdd.map(lambda kv: ((kv[0][0], kv[0][1]), (kv[0][2], kv[1])))\
#     .groupByKey().mapValues(list)
# # map to get top 20 words per (date, sentiment)
# rdd = rdd.map(map_func)  # output similar to second sql output


"""NOT ISN USE!"""
# # counts tweets per day
# rdd_count = predictions.rdd \
#     .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), 1)) \
#     .reduceByKey(lambda a, b: a+b)

# Map reduce to get the top 10 words each day per sentiment
# rdd = predictions.rdd \
#       .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), (row.tokens, 1))) \
#     .reduceByKey(lambda a, b: (a[0] + b[0], a[1] + b[1]))
# rem this in last v
# logger.print_and_log(f"rdd.count(): {rdd.count()}")


"""
Forth approach tested:
"""


def map_func(kv):
    date, prediction, nested_words_count = kv[0][0], kv[0][1], kv[1]
    # flat out the list
    words = []
    count = 0
    for value in nested_words_count:
        words += value[0]
        count += int(value[1])

    # something like thos could also work, but above method is more similar to mrjob
    # words, counts = zip(*[(item[0], item[1]) for sublist in nested_words_count for item in sublist])
    # count = sum(counts)
    words_counted = dict(Counter(words))
    words_counted_top_10 = dict(
        sorted(words_counted.items(), key=lambda x: x[1], reverse=True)[:20])

    return (date, prediction), (count, words_counted_top_10)


# Group by date and sentiment, and count most common words
rdd = predictions.rdd\
    .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), (row.tokens, 1)))
# This line replaced
rdd = rdd.groupByKey().mapValues(list)
rdd = rdd.map(map_func)


# save results
rdd.saveAsTextFile(
    "hdfs:///dat500_prosjekt/spark_batch_results_test3_shortened")


logger.log_program_duration()
