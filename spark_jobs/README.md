## Usage

```bash
## run batch analysis
spark-submit spark_batch_job.py

# see output
hadoop fs -text /dat500_prosjekt/spark_batch_results/part* | less


# stream script (needs two terminals on main node):
python3 twitter_stream.py
spark-submit sentiment_spark_stream.py --conf "spark.driver.extraJavaOptions=-Dlog4jspark.root.logger=WARN,console"
```
