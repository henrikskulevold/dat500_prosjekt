import json
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext, Row
from pyspark.sql.types import DateType, TimestampType
from pyspark.sql.functions import desc, explode, split, regexp_replace, current_date, lit, collect_list, slice, window, col, current_timestamp, date_add
from pyspark.ml import PipelineModel
import os
# Can only run this once. restart your kernel for any errors.


# sc = SparkContext()
# ssc = StreamingContext(sc, 1)
# sqlContext = SQLContext(sc)

# host = "localhost"
# # host = "10.10.9.190"
# port = 50007
# print(f"Try listen to {host}:{port}---")
# socket_stream = ssc.socketTextStream(host, port)
# print("Listens...")

# lines = socket_stream.window(10)


# def map_from_jsonstr(jsonstr):
#     json_dict = json.loads(jsonstr)
#     return json_dict["tweet_id"], json_dict["date"], json_dict["text"]


# print("\n-------before--1----------")
# lines.pprint()
# print("\n-------after----1--------")


# # convert from json
# lines = lines.flatMap(map_from_jsonstr)

# print("\n-------before--2----------")
# lines.pprint()
# print("\n-------after----2--------")

# lines.pprint()


# # lines.transform

# rows = lines.map(lambda line: Row(tweet_id=line[0],
# #                                   date=line[1],
# #                                   text=line[2]
# #                                   ))
# # # rows.transform
# 'df = DataFrame(rows)'
# # # convert to dataframe

# df = lines.map(lambda line: Row(tweet_id=line[0],
#                                 date=line[1],
#                                 text=line[2]
#                                 ))


# # cast to correct types # TODO: mulig dropp
# df = df.withColumn("date", df["date"].cast(DateType()))

# df.show(n=3, truncate=False)


# # Split each line into words
# words = lines.flatMap(lambda line: line.split(" "))

# # Count each word in each batch
# pairs = words.map(lambda word: (word, 1))
# wordCounts = pairs.reduceByKey(lambda x, y: x + y)

# # Print the first ten elements of each RDD generated in this DStream to the console
# wordCounts.pprint()

# # Split each line into words
# words = lines.flatMap(lambda line: line.split(" "))


# ssc.start()             # Start the computation
# ssc.awaitTermination()  # Wait for the computation to terminate

# Load ml model
ML_MODEL_PATH = "../notebooks/models/sentiment_models/v_0"
model = PipelineModel.load(ML_MODEL_PATH)

# create Spark session
spark = SparkSession.builder.appName("twitter_stream").getOrCreate()
spark.sparkContext.setLogLevel("WARN")

# listen to socket
host = "localhost"
port = 50007
lines = spark.readStream.format("socket").option(
    "host", host).option("port", port).load()

# Preprocess the data
# Note: to spare time: got preprocessing from: https://towardsdatascience.com/sentiment-analysis-on-streaming-twitter-data-using-spark-structured-streaming-python-fc873684bfe3
words = lines.select(explode(split(lines.value, "t_end")).alias("word"))
words = words.na.replace('', None)
words = words.na.drop()
words = words.withColumn('word', regexp_replace('word', r'http\S+', ''))
words = words.withColumn('word', regexp_replace('word', '@\w+', ''))
words = words.withColumn('word', regexp_replace('word', '#', ''))
words = words.withColumn('word', regexp_replace('word', 'RT', ''))
words = words.withColumn('word', regexp_replace('word', ':', ''))
words = words.withColumn('word', regexp_replace('word', ':', ''))

# convert to correct format
df = words.withColumn('date', date_add(current_date(), -2))
df = df.withColumn('tokens', split(words.word, " "))
df = df.withColumn('tweet_id',  lit(10))

# predict using ml model
predictions = model.transform(df)
predictions = predictions.select("tweet_id", "date", "tokens", "prediction")


# def map_func(kv):
#     date, prediction, word_count_list, = kv[0][0], kv[0][1], kv[1]
#     words_counted_top_10 = dict(
#         sorted(word_count_list, key=lambda x: x[1], reverse=True)[:20])
#     return (date, prediction), words_counted_top_10


# rdd = predictions.rdd \
#     .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), row.tokens))\
#     .flatMap(lambda kv: (((kv[0][0], kv[0][1], word), 1) for word in kv[1]))\
#     .reduceByKey(lambda a, b: a + b)\
#     .map(lambda kv: ((kv[0][0], kv[0][1]), (kv[0][2], kv[1])))\
#     .groupByKey().mapValues(list)\
#     .map(map_func)


# def foreach_batch_function(df, epoch_id):
#     exploded_df_on_words = (
#         df.withColumn("word", explode(predictions.tokens))
#         .groupBy(["date", "prediction", "word"])
#         .count()
#         .sort("count", ascending=False)
#     )
#     grouped_date_pred = exploded_df_on_words.groupBy(
#         ["date", "prediction"]).agg(collect_list("word"))
#     grouped_top_10 = grouped_date_pred.withColumn(
#         "top_20_words", slice("collect_list(word)", start=1, length=20))
#     grouped_top_10_filtered = grouped_top_10.select(
#         ["date", "prediction", "top_20_words"])
#  .withColumn('timestamp', unix_timestamp(col('EventDate'), "MM/dd/yyyy hh:mm:ss aa").cast(TimestampType())) \

def foreach_batch_function(df, id):
    grouped_count = df.groupBy(["date", "prediction"]).count()
    grouped_count.show(n=5, truncate=False)
    print(grouped_count.count())


# predictions = predictions.withColumn('timestamp', current_timestamp())

# predictions_grouped = predictions\
#     .withWatermark("timestamp", "5 minutes") \
#     .groupBy(["date", "prediction"]).count()
#     #
query = predictions.writeStream.queryName("all_tweets")\
    .outputMode("append").format("console")\
    .foreachBatch(foreach_batch_function)\
    .start()

# .trigger(processingTime='10 seconds')\

# query.
# query = query.rdd \
#     .map(lambda row: ((row.date, 'positive' if row.prediction == 4 else 'negative'), row.tokens))\
#     .flatMap(lambda kv: (((kv[0][0], kv[0][1], word), 1) for word in kv[1]))\
#     .reduceByKey(lambda a, b: a + b)\
#     .map(lambda kv: ((kv[0][0], kv[0][1]), (kv[0][2], kv[1])))\
#     .groupByKey().mapValues(list)\
#     .map(map_func)

query.awaitTermination()


# words.show(n=3, truncate=False)

# words.rdd.saveAsTextFile("/tmp/tweets")


# query1 = words.collect().(print) \
#     .writeStream \
#     .format("console") \
#     .start()


# words.printSchema()
# query1.awaitTermination()

# query = words.writeStream.queryName("all_tweets")\
#     .outputMode("append").format("parquet")\
#     .option("path", "./parc")\
#     .option("checkpointLocation", "./check")\
#     .trigger(processingTime='60 seconds').start()
# query.awaitTermination()


# # Load ml model
# # TODO: rem this check, only used when debugging locally
# if os.name == 'nt':
#     ML_MODEL_PATH = "../notebooks/models/sentiment_models/v_0"
# else:
#     ML_MODEL_PATH = "./models/sentiment_models/v_0"

# model = PipelineModel.load(ML_MODEL_PATH)
