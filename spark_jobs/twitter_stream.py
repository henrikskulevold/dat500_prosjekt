# imports
import json
import tweepy
from tweepy.client import Response
import socket

# setting up the keys
bearer_token = '<bearer_token>'

consumer_key = '<consumer_key>'
consumer_secret = '<consumer_secret>'
access_token = '<access_token>'
access_secret = '<access_secret>'


class TweetListener(tweepy.StreamingClient):
    # A listener handles tweets are the received from the stream.

    def __init__(self, c_socket: socket, bearer_token, return_type=Response,
                 wait_on_rate_limit=False, **kwargs):
        super().__init__(bearer_token,  return_type=return_type,
                         wait_on_rate_limit=wait_on_rate_limit, **kwargs)

        self.count = 0
        self.socket = c_socket

    def on_connect(self):
        print("Connected--")

    def on_tweet(self, tweet):
        """This is called when a Tweet is received.

        Parameters
        ----------
        status : Tweet
            The Tweet received
        """
        self.count += 1

        if (self.count % 100 == 0):
            print("COUNT-----------------------", self.count)

        # lang en did not work as espected (very few got through)
        if tweet.lang == "en" or True:
            # print("tweet:")
            # print(tweet.text)

            body = {
                'tweet_id': tweet.id,
                'text': tweet.text,
                "date": tweet.created_at,
            }
            data = json.dumps(body)

            self.socket.send(tweet.text.encode('utf-8'))

            #self.socket.send(bytes(data, encoding="utf-8"))

    def on_includes(self, includes):
        """This is called when includes are received.

        Parameters
        ----------
        includes : dict
            The includes received
        """
        print("includes:")
        print(includes)

    def on_errors(self, errors):
        """This is called when errors are received.

        Parameters
        ----------
        errors : dict
            The errors received
        """
        print("Received errors: %s", errors)

# printing all the tweets to the standard output
#auth = tweepy.OAuth1UserHandler(consumer_key, consumer_secret)
#auth.set_access_token(access_token, access_secret)


def setRules(stream: TweetListener):
    rules = stream.get_rules()
    if (rules.data is not None):
        ids_to_delete = [*map(lambda x: x.id, rules.data)]
        print("ids_to_delete:", ids_to_delete)

        if (len(ids_to_delete) > 0):
            res = stream.delete_rules(ids_to_delete)
            print("res delete_rules")
            print(res)

    res = stream.add_rules(
        [tweepy.StreamRule(value="covid19"),
         tweepy.StreamRule(value="coronavirus"),
         tweepy.StreamRule(value="covid-19"),
         tweepy.StreamRule(value="covid"),
         tweepy.StreamRule(value="corona"),
         tweepy.StreamRule(value="coronavirus"),
         tweepy.StreamRule(value="coronacrises"),
         tweepy.StreamRule(value="quarantine"),
         tweepy.StreamRule(value="lockdown"),
         tweepy.StreamRule(value="wearamask"),
         tweepy.StreamRule(value="stayathomeorder"),
         tweepy.StreamRule(value="Social_Distancing")]
    )

    # res = stream.add_rules([
    #    tweepy.StreamRule(value="dog has:images", tag="dog pictures"),
    #    tweepy.StreamRule(value="cat  has:images -grumpy", tag="cat  pictures")
    # ])
    print("res add_rules")
    print(res)

    rules = stream.get_rules()
    print("rules")
    print(rules)


# stream.disconnect()
if __name__ == "__main__":
    # server (local machine) creates listening socket
    import os
    if os.name == 'nt':
        host = "127.0.0.1"
    else:
        host = "10.10.9.190"
    port = 50007

    with socket.socket() as s:
        try:
            s.bind((host, port))
            s.listen(4)
            print("waiting for connection at host:", host, "port: ", port)
            c_socket, addr = s.accept()
            print("Received request from: " + str(addr))
            stream = TweetListener(
                c_socket=c_socket, bearer_token=bearer_token)
            setRules(stream)
            print("START stream")
            # stream.filter()
            stream.sample()
            print("end stream")

        except BrokenPipeError:
            print("BrokenPipeError IGNORED")
            pass
        except OSError as msg:
            s.close()
            print("Bind failed. Error Code : " +
                  str(msg[0]) + " Message " + msg[1])
        except KeyboardInterrupt as msg:
            s.close()
            print("Interupted by Henrik.")
