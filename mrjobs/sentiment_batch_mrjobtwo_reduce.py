from mrjob.job import MRJob
from mrjob.step import MRStep
import os
from collections import Counter
from mrjob_wrapper import wrap_mrjob

RUNS_LOCALLY = False


# helper funcs
def get_words_scores():
    """
        cwd = Path.cwd()
    local_path = cwd / "assets/AFINN-en-165.txt"
    """
    local_path = os.path.join(os.path.dirname(
        __file__), "./assets/AFINN-en-165.txt")
    cluster_path = "./AFINN-en-165.txt"
    file_path = local_path if RUNS_LOCALLY else cluster_path
    with open(file_path) as f:
        words_dict = {}
        for line in f:
            word, score = line.split('\t')
            words_dict[word] = int(score)

    return words_dict


class MRSentimentBatch(MRJob):
    WORDS_SCORE_DICT = {}

    def _score_word(self, word):
        if word in self.WORDS_SCORE_DICT:
            return self.WORDS_SCORE_DICT[word]
        else:
            return 0

    def steps(self):
        return [MRStep(mapper_init=self.mapper_init,
                       mapper=self.mapper,
                       combiner=self.combiner,
                       reducer=self.reducer
                       ),
                MRStep(reducer=self.reducer_top_10)
                ]

    def mapper_init(self):
        # TODO: tets dette?
        # ML_MODEL_PATH = "../models/sentiment_models/v_0"
        # model = LogisticRegressionModel.load(ML_MODEL_PATH)

        self.WORDS_SCORE_DICT = get_words_scores()

    def mapper(self, _, line):
        tweet_id, value = line.split('\t')

        # remove special chars mrjob doesnt encode
        date_time,  words_str = value.split(",[")
        date_time = date_time.replace("[", "").replace("\"", "")
        date = date_time.split("T")[0]

        words = words_str.split(",")
        words = [value.replace("\"", "").replace("]", "") for value in words]

        scores = [self._score_word(word) for word in words]
        sentiment = "positive" if sum(scores) > 0 else "negative"

        for word in words:
            yield (date, sentiment, word), 1

    def combiner(self, key, values):
        yield(key, sum(values))

    def reducer(self, key, values):
        word_count = (key[2], sum(values))
        # (date, sentiment), ('sad', 24 )

        yield((key[0], key[1]), word_count)

    def reducer_top_10(self, key, word_count):
        word_count_list = list(word_count)

        words_counted_top_10 = dict(
            sorted(word_count_list, key=lambda x: x[1], reverse=True)[:20])

        # note: får ikke count på denne metoden i guess

        yield key, words_counted_top_10


if __name__ == '__main__':
    wrap_mrjob(MRSentimentBatch)
