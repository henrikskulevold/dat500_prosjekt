from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob_wrapper import wrap_mrjob
from text_cleaner import clean_and_tokenize_text


class MRMultilineBatchPreprocessing(MRJob):
    def steps(self):
        return [
            MRStep(
                mapper_init=self.multiline_mapper_init,
                mapper=self.multiline_mapper,
            ),
            MRStep(
                #   mapper_init=self.mapper_init,
                mapper=self.mapper,
            )
        ]

    """
    Step: Convert multilined tweets to one line
    """

    def _clear_Tweetbody(self):
        self.in_tweet = False
        self.tweet = []
        self.tweet_date = None
        self.tweet_id = ''

    def multiline_mapper_init(self):
        self._clear_Tweetbody()

    def multiline_mapper(self, _, line):
        line = line.strip()
        line_elements = line.split(',')
        start = line.find(',"')
        end = line.find('",')

        # helper func: check if start of line is the tweet id
        def line_starts_with_tweet_id():
            return (line.find('124') == 0 or line.find('125') == 0) and len(line.split(',')[0]) == 19

        # Skip tweet if not english
        if (end != -1 and line[-3:] != ",en"):
            self._clear_Tweetbody()
            return

        if line_starts_with_tweet_id() and start != -1 and len(line_elements) > 3:
            self.tweet_id = line[0:19]
            self.tweet_date = line_elements[2]
            if end == -1:
                self.tweet.append(line[start+2:end])
                self.in_tweet = True
            # hvis slutten finnes
            else:
                self.tweet.append(line[start+2:end])
                yield self.tweet_id, (self.tweet_date,  ' '.join(self.tweet))
                self._clear_Tweetbody()

        elif end != -1 and self.in_tweet:
            self.tweet.append(line[0:end])
            yield self.tweet_id, (self.tweet_date,  ' '.join(self.tweet))
            self._clear_Tweetbody()

        elif self.in_tweet:
            self.tweet.append(line)

    """
    Step: Clean and tokenize text
    """

    def mapper(self, tweet_id, data):
        date, tweet = data
        tokens = clean_and_tokenize_text(tweet)
        yield tweet_id, (date, tokens)


if __name__ == '__main__':
    wrap_mrjob(MRMultilineBatchPreprocessing)
