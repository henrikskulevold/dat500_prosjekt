from mrjob.job import MRJob
from mrjob.step import MRStep
import os
from collections import Counter
from mrjob_wrapper import wrap_mrjob

RUNS_LOCALLY = False


# helper funcs
def get_words_scores():
    """
        cwd = Path.cwd()
    local_path = cwd / "assets/AFINN-en-165.txt"
    """
    local_path = os.path.join(os.path.dirname(
        __file__), "./assets/AFINN-en-165.txt")
    cluster_path = "./AFINN-en-165.txt"
    file_path = local_path if RUNS_LOCALLY else cluster_path
    with open(file_path) as f:
        words_dict = {}
        for line in f:
            word, score = line.split('\t')
            words_dict[word] = int(score)

    return words_dict


class MRSentimentBatch(MRJob):
    WORDS_SCORE_DICT = {}

    def _score_word(self, word):
        if word in self.WORDS_SCORE_DICT:
            return self.WORDS_SCORE_DICT[word]
        else:
            return 0

    def mapper_init(self):
        # TODO: tets dette?
        # ML_MODEL_PATH = "../models/sentiment_models/v_0"
        # model = LogisticRegressionModel.load(ML_MODEL_PATH)

        self.WORDS_SCORE_DICT = get_words_scores()

    def mapper(self, _, line):
        tweet_id, value = line.split('\t')

        # remove special chars mrjob doesnt encode
        date_time,  words_str = value.split(",[")
        date_time = date_time.replace("[", "").replace("\"", "")
        date = date_time.split("T")[0]

        words = words_str.split(",")
        words = [value.replace("\"", "").replace("]", "") for value in words]

        scores = [self._score_word(word) for word in words]
        sentiment = "positive" if sum(scores) > 0 else "negative"

        yield (sentiment, date), (1, words)

    def combiner(self, key, values):
        words = []
        count = 0
        for value in values:
            words += value[1]
            count += int(value[0])
        yield key,  (count, words)

    def reducer(self, key, values):
        words = []
        count = 0
        for value in values:
            words = [*words, *value[1]]
            count += int(value[0])

        # counte alle ord
        words_counted = dict(Counter(words))
        words_counted_top_10 = dict(
            sorted(words_counted.items(), key=lambda x: x[1], reverse=True)[:10])

        yield key, (count, words_counted_top_10)


if __name__ == '__main__':
    wrap_mrjob(MRSentimentBatch)
