## Usage:

### Batch preprocessing:

```bash
# dwnload batch file to cluster
gdown https://drive.google.com/u/0/uc?id=1OusWdWaEANrdkO-euilbcggPp7dp3iUT&export=download

###
# run covid 19 batch preprocessing: On full dataset
python3 preprocessing_covidtweets_mrjob.py  \
--conf-path mrjob.preprocess.conf \
--hadoop-streaming-jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar \
-r hadoop hdfs:///dat500_prosjekt/covid_tweets.csv \
--output-dir hdfs:///dat500_prosjekt/covid_tweets_output --no-output

# list file
hadoop fs -text /dat500_prosjekt/covid_tweets_output/part* | less
```

### Training data preprocessing:

```bash
# Run job of full dataset
python3 preprocessing_mrjob.py  \
--conf-path mrjob.preprocess.conf \
--hadoop-streaming-jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar \
-r hadoop hdfs:///dat500_prosjekt/training_data.csv \
--output-dir hdfs:///dat500_prosjekt/training_data_output --no-output


# list file
hadoop fs -text /dat500_prosjekt/training_data_output/part* | less
```

### Sentiment batch mrjob

```bash
# run covid 19 batch job on shortened output With 2 reducers
python3 sentiment_batch_mrjobtwo_reduce.py  \
--conf-path mrjob.batch.conf \
--hadoop-streaming-jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar \
-r hadoop hdfs:///dat500_prosjekt/covid_tweets_output \
--output-dir hdfs:///dat500_prosjekt/batch_job_output_v2 --no-output

# list file
hadoop fs -text /dat500_prosjekt/batch_job_output_v2/part* | less


# run covid 19 batch job on full output
python3 sentiment_batch_mrjob.py  \
--conf-path mrjob.batch.conf \
--hadoop-streaming-jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar \
-r hadoop hdfs:///dat500_prosjekt/covid_tweets_output \
--output-dir hdfs:///dat500_prosjekt/batch_job_output --no-output

# list file
hadoop fs -text /dat500_prosjekt/batch_job_output/part* | less

```

### remove an poutput file:

```bash
hadoop fs -rm -R /dat500_prosjekt/<file name>
```
