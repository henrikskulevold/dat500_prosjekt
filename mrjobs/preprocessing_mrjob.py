from mrjob.job import MRJob
from mrjob_wrapper import wrap_mrjob
from text_cleaner import clean_and_tokenize_text


class MRTrainingDataPreprocessing(MRJob):
    """
    This MRJob is used to preprocess training data to be used in an ml model.

    Limitations: caannot use downloaded nltk data

    Description of input (from http://help.sentiment140.com/for-students):
        - It contains the following 6 fields:
        - target: the polarity of the tweet (0 = negative, 2 = neutral, 4 = positive)
        - ids: The id of the tweet ( 2087)
        - date: the date of the tweet (Sat May 16 23:58:44 UTC 2009)
        - flag: The query (lyx). If there is no query, then this value is NO_QUERY.
        - user: the user that tweeted (robotickilldozr)
        - text: the text of the tweet (Lyx is cool)
    """

    def mapper(self, key, line):
        if len(line.split(',')) == 6:
            target, tweet_id, created_at, query, user, text = line.split(',')

            # remove " from target ans tweet_id
            target = target.replace('"', '')
            tweet_id = tweet_id.replace('"', '')

            # clean text and tokenize
            tokens = clean_and_tokenize_text(text)

            yield tweet_id, (target, tokens)


if __name__ == '__main__':
    wrap_mrjob(MRTrainingDataPreprocessing)
