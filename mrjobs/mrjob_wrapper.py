

from datetime import datetime
import os
import socket
import sys
import time


def wrap_mrjob(mrjob):
    start = time.time()

    log_folder = f"./logs/{mrjob.__name__}"
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)

    mrjob.run()

    end = time.time()
    elapsed = end - start
    sys.stderr.write(f"\n\nTime elapsed: {elapsed} seconds\n")

    with open(log_folder + "/time_elapsed.log", "a") as log:
        log.write(f"\n-- NEW RUN, datetime: {datetime.now()} -- \n")
        log.write("New test for node: " + socket.gethostname() + " ")
        log.write(str(elapsed) + "\n")
